In order to run the scraper simply put the subreddits which you would like to have scraped into the toBeScraped.txt file.
Alternatively you could also create your own text file with the subreddits you'd like to hhave scraped listed in them.
List the subreddits in the following format:

subreddit
subreddit
subreddit

Example:

nosleep
Askreddit
programming

Once you have your text file created run the script like so:

python3 scraper.py (filepath)

As soon as the script has launched it will prompt you to enter the number of submissions you would like to scraper per subreddit.
Note that typing in 0 will removing the imposition of a limit altogether.

Happy scraping ! :]