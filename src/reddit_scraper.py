import praw
import sys
import logging
import os
# import getopt


# run with: python[version] reddit-scraper.py subredditsTobeScrapedFile.txt

# This contains all the info we need in order to perform the scraping
# including all the necessary credentials to interact with reddit
# as well as a list of subreddits we oughta scrape
class scrapingPillar:
    client_secret = 'USKqNjAJ6mMNQmZujmW2KI4EQl4'
    client_id = 'DBvqP5Pumlke3g'
    password = 'Yourmom'
    user_agent = 'reddit-scraper'
    username = 'RecastAIisAI'


def create_reddit_instance(scrapingPillar):
    reddit = praw.Reddit(client_id=scrapingPillar.client_id,
                         client_secret=scrapingPillar.client_secret,
                         password=scrapingPillar.password,
                         user_agent=scrapingPillar.user_agent,
                         username=scrapingPillar.username)
    reddit.read_only = True
    return reddit


# This takes the file(argument) and makes a list of scrapable subs out of it
def get_list_of_subs(argv):
    scrapingPillar.subs = sys.argv
    my_file = open(sys.argv[1], "r")
    subreddits = my_file.read()
    my_file.close()
    return subreddits.split('\n')


# creating an instance for the specified subreddits in a loop
def get_subreddit_instance(scrapingPillar, i):
    return scrapingPillar.reddit.subreddit(scrapingPillar.subs[i])


def scraper_main(argv):
    try:
        i = 0
        # instantiating a reddit instance
        # getting a list of the subs I need
        scrapingPillar.subs = get_list_of_subs(argv)
        print("'\n", "Scraping the subreddits: ")
        print("\n", scrapingPillar.subs)
        filename = str(input("\nPlease type in the name of the output file: "))

        if (os.path.isfile(filename)):
            dataset = open(filename, 'a')
        else:
            dataset = open(filename, 'w')
        submissionLimit = int(input("\nHow many submission per subreddit would you \
like to scrape? Type in 0 if you don't want a limit: "))
        if submissionLimit == 0:
            submissionLimit = None
        scrapingPillar.reddit = create_reddit_instance(scrapingPillar)
        while i < len(scrapingPillar.subs):
            subreddit = get_subreddit_instance(scrapingPillar, i)
            print(scrapingPillar.subs[i])
            # there is no limit to the scraping of the subreddits we need to do
            # this nested double loop takes care of a single subreddit
            for submission in subreddit.top(limit=submissionLimit):
                print(submission.title)
                dataset.write(submission.title)
                dataset.write('\n')
                submission.comments.replace_more(limit=None, threshold=0)
                for comment in submission.comments.list():
                    dataset.write('\n')
                    dataset.write(comment.body)
            print(i)            
            i = i + 1
        dataset.close()
    except:
        print("Error in the main function! ")


scraper_main(scrapingPillar)
